\section{Feedback Received by Fellow Teachers}
Sections \ref{sect:autotests} and \ref{sect:github} give an introduction to
creating automated tests and to using these tests for automated grading with
GitHub Classroom. Since the target audience for these two sections are fellow
computer science teachers, it is important to make the content clear and
easy to understand for them. To achieve this, qualitative feedback of
selected colleagues is collected that can later be incorporated in a
designated manual that is distributed to a wider audience.


\subsection{Methodology}
In the spirit of open source software, the manuscript of this bachelor's
thesis is available using the viral creative commons CC-BY-NC-SA license.
The repository containing the source files is publicly accessible at
\url{https://gitlab.com/senarclens/paedak-bachelor-thesis}. Anyone is free
to propose future improvements by means of creating a merge request with
suggested improvements.

In particular, a small selection of colleagues has been asked for feedback.
Their names and a very short summary of their educational achievements are
listed in the next subsection. They have been selected to represent a broad
spectrum from people that recently became computer science teachers to those
soon to retire. Also, their knowledge with regard to git varies greatly.

Each of the selected colleagues was provided this manuscript and asked to
provide consise written suggestions for improving sections \ref{sect:autotests}
and \ref{sect:github}. Their qualitative assessment and critique is
summarized in subsection
\ref{ssect:feedback} below.
To avoid academic misconduct, their feedback can
only be incorporated in a separately created manual containing updated versions
of the prior two sections of this publication.


\subsection{Selection of Teachers for Reviewing the Draft}
Below are brief résumés of four fellow computer science teachers (in
alphabetical order).


\subsubsection*{Edgar Neukirchner}
After completing his studies at Graz University of Technology with a
degree in electronic and communication engineering, Edgar worked as a
hardware and software designer at Honeywell, Seibersdorf Research and
Austria Microsystems. Since 2000, he teaches full time at HTL Bulme Graz
Gösting.


\subsubsection*{Michael Kahr}
Michael received a master of science in Business Administration from the
University of Vienna, and another one in Business Education from the
Vienna University of Economics and Business. In 2021, he further
received a PhD in Operations Research from the Department of Statistics
and Operations Research, University of Vienna. The research focus during
the PhD was on quantitative social network analysis, particularly, on
influence maximization problems. Since then, he worked as postdoctoral
fellow at the Research Network Data Science (Vienna), the Johannes
Kepler University (Linz), and the Karl-Franzens-University (Graz). He is
teaching topics related to business, mathematics, optimization, and
algorithm design. His research is published in leading scientific
journals related to (mathematical) optimization.

\subsubsection*{Stefan Kummer}
After completing a master of science at Graz University of Technology, Stefan
took on multiple roles in renowned international companies including
NXP Semiconductors and Infineon Technologies. Since September 2017 he serves as
full time teacher in the Department of Electronics and Technical Computer
Science at HTL Bulme. He gives lectures in a variety of technical subjects
and has experience with
automated assessment of C and C++ tasks, although Stefan has not yet used them
on a regular basis due to the high initial effort in creating suitable
assignments.


\subsubsection*{Thomas Winkler}
After graduating from Graz University of Technology with a master of science in
telematics, Thomas worked as a software engineer at SPIELO International and
Dewetron for almost 14 years. Since September 2018, he works as a teacher for
applied computer science at HTL Bulme Graz Gösting.




\subsection{Collected Feedback}
\label{ssect:feedback}
After studying this theisis, the teachers and academics introduced above
provided their
valuable feedback which is listed below. These experts' comments can be used to
improve sections \ref{sect:autotests} and \ref{sect:github} of this report in
a separate publication.


\subsubsection*{Edgar Neukirchner}
Edgar Neukirchner is one of the most senior computer science teachers at HTL
Bulme. He is still very enthusiastic and keen on new technological approaches
to improve teaching. His feedback is:

\begin{quotation}
This thesis deals with an extremely relevant topic that is only sparsely
covered in other educational literature. Programming can only be learned
by constantly working on practical examples. In everyday teaching, it is
almost impossible to evaluate the submitted solutions by hand.

In his work, the author presents tools for automated software testing
that have proven themselves not only in the academic field, but also in
harsh industrial environments. He demonstrates a profound understanding
of the subject. Moreover, the thesis goes beyond theoretical discussions
by incorporating real-world scenarios and immediately applicable code
examples. This practical approach adds immense value, as it equips
readers with actionable insights that can be directly applied to enhance
the quality of education in a technical setting.
\end{quotation}


\subsubsection*{Michael Kahr}
The author of the thesis had the pleasure of co-teaching a course with Michael
at University of Graz in the fall 2023 term. During our lectures, we already
applied some of the approaches lined out in the paper at hand which gives
Michael a lot of hands on experience. His insights are:

\begin{quotation}
It was my pleasure to read this Bachelor's thesis whose quality is far
beyond the average. It gives a good introduction, and practical insights
to get started with automated grading. In Fall 2023, I taught the Python
programming language applied to computational management science at the
University of Graz, and used an automated grading framework for the
first time. I can confirm the benefits from automated testing in
practice, as described in the thesis. From teacher perspective, I
consider the most important one the reduced time for grading (and
debugging) student work. From student perspective, I consider the most
important one immediate feedback. I also used GitHub Classroom in my
lecture but was not aware that including automated testing is as simple
as described in Section 4. I will consider using this option in my
upcoming lectures.
\end{quotation}


\subsubsection*{Stefan Kummer}
Stefan not only took the time to read and understand the content of this
thesis, but even applied it in a senior class. Furthermore, Stefan asked the
class to provide some feedback on their exercise. One thing to bear in mind is
that using GitHub classroom requires training students to properly use
git, ideally before adding the additional complexity of a programming
assignment. Even though the students received a brief introduction of git
during the prior school year, an evaluation by the students after a single
lesson is likely to
lead to distorted results. It would be better to compare the learning
results in two larger sets of classes - one with and one without the use of
GitHub. A large number of participants and a proper control group are of utmost
importance to remove at
least some biases. The amount of work required for doing so would, however,
require a separate publication.

Nevertheless, Stefan's empirical results already provide very useful insights
that can and should be taken into account. Furthermore, issues and potential
improvements for the provided example were uncovered thanks to Stefan's
experiment. He writes:

\begin{quotation}
In addition to the statements made in this bachelor's thesis, which I find
entirely comprehensible, I would like to share my insights and expertise
regarding the automated evaluation of programming tasks. The focus of this
work is on the Github Classroom environment, which was new to me at the
beginning of this review. The use of test cases and concepts such as
continuous integration (CI) or version control (VC) to prevent
non-functional integrations is familiar to me from my experience in working
on larger software projects. One of the systems for automated grading of
programming tasks named Virtual Programming Lab (VPL), which I have used so
far, is based on using test cases in a very similar way as with this system.
When I evaluated the concept of CI/VC in the year 2022 for use in teaching,
the tools available to me at that time lacked the necessary interfaces for
teachers, for example to provide tasks to students, let them test their
solutions automatically, monitor the processing steps and assess the
results. All these points have now been addressed by Github Classroom.

I agree that the use of Github involves an initial barrier for students. In
my opinion, this additional effort is fine, because the use of a code
repository system is necessary knowledge for our graduates. However, I think
that this hurdle means that applying a system based on Github in the first-
and possibly even the second-year classes does not make sense or might only
be possible to a limited extent. In my view, one main reason for this
restriction is the complex output of the grading system, which contains the
entire build process and the resulting error messages and is the only
feedback available to the students. This output consists of around 200 text
lines. From my teaching experience, compiler output is difficult to
understand for most students and the correct interpretation of error
messages in relation to possible sources of error is often not clear. When
executing this autograding process, error messages arise that do not easily
allow conclusions to be drawn about the type of error and only represent the
return values of the test case. To what extent this deficiency can be
improved by adapting or expanding the test cases is beyond my knowledge and
was already a hurdle for me when using VPL.

To get feedback on this new system from students, I prepared an assignment
with a simple programming task and carried it out with a very experienced
class from the final year who had already worked with Github in the past -
but not with Github Classroom yet. The preparation from the teacher's
perspective was very easy because the example already existed and only a new
classroom had to be created for the class containing the names of the
students. This can even be omitted for future tasks with the same class.
Linking the task was completed in a short time.

Thirteen students worked on the task, but only four of them (almost 31\%)
successfully passed. Unsurprisingly, the names of these students coincide
with the better half in the grade ranking of the class. The students spent a
little more than one teaching unit working on the task, which is a bit out
of the ordinary for such a simple task.

The reason for the poor performance and the long working time was that this
task also tested so-called corner cases, which the students obviously did
not consider in the first draft (no attention was paid to the range of
values of the data types used or the execution time of the program).

In order to get a deeper insight into the students' experience with this
system, I created a survey with 7 questions, which was filled out by 11 of
the 13 students immediately after completing the task. The following
statements emerged:

\begin{enumerate}
  \item The task was easy from the students' perspective (grade 2.0 on a school
    grading scale, with 1 for "very easy" to 5 for "very difficult").
  \item Github Classroom is rated as easy to use (grade 2.5).
  \item The opportunity of the environment to develop the program with the
    preferred programming environment (in addition to the suggested browser
    integration of VS Code) has been utilized by some students.
  \item The students worked iteratively on a solution and made further attempts
    after initial failed results of the automated tests. On average, 5 attempts
    were made (minimum 2, maximum 8).
  \item The average score achieved was 2.3 out of 5 (46\%). 4 students scored 0
    points, 5 students reached 2 points each and 4 students achieved all 5
    points.
  \item The assessment from the automated assessment is clear to the students.
  \item Surprisingly, when asked about their perception of the automated grading
    format, students provided predominantly negative feedback: only 1 student
    appraised the system as positive (grade 2 according to the school grading
    system), 3 rated it neutral (grade 3), 6 were negative about it (grade 4)
    and one person judged it very negative (grade 5). This results in an
    average grade of 3.6.
\end{enumerate}

I attribute some of this negative feedback to the rather negative results,
which, as already described, arise from the ignored corner cases. It would
require further assignments and feedback rounds to gain more insight into
the students' perspective. From my perspective as a teacher, this test run
was very promising because the positive effects of an automated evaluation
system for tasks in the programming area were all met. The most important of
these, in my view, is the changed role of myself, which is changing from a
classical teacher role as an evaluator to a coach and supporter. Other
positive effects include efficient, immediate, consistent, and fair or
transparent evaluation. I see the effort involved in creating motivating
examples with meaningful outputs of test cases as a potential problem. Here
it will be necessary to turn the cryptic messages from autograding to
understandable information for the students. It would be desirable to output
hints as complete (German) sentences where errors in the entered solution
are found. In my opinion, this is essential for using such a system in
first-year classes. For higher classes, I see the lack of such information
as less critical, since with advanced experience students may be required to
independently interpret faulty test cases by studying the code of the
respective test cases.

In my view, the only obstacle to the widespread adoption of this system is
the shortage of well-prepared assignments. I am confident that motivated
teachers, including myself, will soon tackle and resolve this challenge.
\end{quotation}

\subsubsection*{Thomas Winkler}
After carefully reading the publication at hand, Thomas summarizes his
impressions as follows:

\begin{quotation}
  After some years of unsatisfactory search for a proper solution
  of one of the most crucial problems when teaching software development,
  this thesis finally provides an excellent hands-on introduction to
  Github Classroom, probably the most powerful framework for autograding
  programming assignments at the moment.

  The thesis not only gives a good insight into unit
  testing for the three most common languages used when teaching computer
  programming. It also provides profound sample configurations for using them
  with Github Classroom's auto grading capabilities. Further, the author
  provides useful ideas to get even more fine-grained results from the test
  runs.
\end{quotation}
