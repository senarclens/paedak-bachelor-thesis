# Automated Grading of Programming Assignments

Bachelor thesis submitted to University College of Teacher Education Styria.

Compile the project either by typing

```shell
make
```

or by using a proper $\LaTeX$ development environment like Codium with
the LaTeX Workshop extention.
